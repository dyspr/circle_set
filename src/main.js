var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var limit = 0.3
var pos = []
var size = []
var numOfCircles = 6
var maxSize = 0.3
var minSize = 0.05

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < numOfCircles; i++) {
    pos.push([Math.random() * limit * 2 - limit, Math.random() * limit * 2 - limit])
    size.push(Math.random() * (maxSize - minSize) + minSize)
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < pos.length; i++) {
    push()
    translate(windowWidth * 0.5 + pos[i][0] * boardSize, windowHeight * 0.5 + pos[i][1] * boardSize)
    noFill()
    fill(255 * (i / pos.length))
    stroke(255)
    strokeWeight(boardSize * 0.005)
    ellipse(pos[i][0], pos[i][1], boardSize * size[i])
    point(pos[i][0], pos[i][1])
    pop()
  }

  for (var i = 0; i < pos.length; i++) {
    for (var j = i + 1; j < pos.length; j++) {
      noFill()
      stroke(255)
      strokeWeight(boardSize * 0.005)
      strokeCap(ROUND)
      line(windowWidth * 0.5 + pos[i][0] * boardSize, windowHeight * 0.5 + pos[i][1] * boardSize, windowWidth * 0.5 + pos[j][0] * boardSize, windowHeight * 0.5 + pos[j][1] * boardSize)
    }
  }

  if (frameCount % 2 === 0) {
    if (pos.length > 3 + (numOfCircles - 2) * abs(sin(frameCount * 0.01))) {
      pos = pos.splice(1)
      size = size.splice(1)
    } else {
      pos.push([Math.random() * limit * 2 - limit, Math.random() * limit * 2 - limit])
      size.push(Math.random() * (maxSize - minSize) + minSize)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
